#include <iostream>
#include<unistd.h>


using namespace std;
//funcion que genera un numero aleatorio entre 1 y 100 (serán los datos)
int dataRandom(){
    int dr;
    dr = 1 + rand() % (100-1);
    //printf("dato = %d/n",dr);
    return dr;
}
//funcion que genera un ritmo de muestreo aleatorio entre 2 ms y 1 s y retorna un puntero a rmr
int RitmoMuestreoRandom(){
    int rmr;
    srand((unsigned)time(NULL));
    rmr = 2 + rand() % (1000-2); //generamos numeros entre 2 ms y 1000 ms
    //printf("Ritmo de Muestreo = %d/n",rmr);
    return rmr;
}
//función temporizador
int main(){
    unsigned int CH1, CH2, CH3;
    unsigned int Ts; //periodo de muestreo Ts = 1/Fs, con Fs entre 1 y 500 Hz (ver funcion RitmoMuestreoRandom)
    unsigned int tiempoMuestreo = 3600; //tiempo durante el cual se generan y almacenaran muestras (en segundos)
    time_t ahora = time(NULL);
    time_t inicio = time(NULL);
    FILE *archivotexto = fopen("AudioData.txt","w"); //archivo de tipo fichero
    if(archivotexto == NULL){ // aviso en caso de no poder abrirse el archivo
        printf("No se ha podido abrir el archivo");
        exit(1);
    }
    srand((unsigned)time(NULL)); //sembramos la semilla fuera del loop por que sino tendriamos el mismo numero aleatorio para los 3 canales

    while(difftime(ahora,inicio) <= tiempoMuestreo){
        ahora = time(NULL);
        Ts = RitmoMuestreoRandom();
        CH1 = dataRandom();
        CH2 = dataRandom();
        CH3 = dataRandom();
        fprintf(archivotexto,"%d\t%d\t%d\n",CH1,CH2,CH3);
        cout << CH1 << endl;
        usleep(1000*Ts);
    }
    fclose(archivotexto);
    return 0;
}

/*
int main(int argc, char *argv[]){
    float audio;
    cout << "ingrese periodo de muestreo en segundos " << endl;
    string s;
    getline(cin, s);
    int segundos;
    if(istringstream(s) >> segundos) audio = getRandom();
    system("PAUSE");
    return 0;
}
*/



